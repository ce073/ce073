#include <stdio.h>
#include <stdlib.h>
#include <math.h>
void add(int *n1, int *n2);
void sub(int *n1, int *n2);
void mul(int *n1, int *n2);
void div(int *n1, int *n2);

int main()
{
    int num1, num2;
	printf("Enter Two numbers: ");
	scanf("%d",&num1);
	scanf("%d",&num2);
    add( &num1, &num2);
	sub( &num1, &num2);
	mul( &num1, &num2);
	div( &num1, &num2);
    return 0;
}

void add(int* n1, int* n2)
{
    int sum=0;
	sum= (*n1) + (*n2);
	printf("\nsum = %d",sum);
}
void sub(int* n1, int* n2)
{
    int dif=0;
	dif= abs((*n1) - (*n2));
	printf("\ndif = %d",dif);
}
void mul(int* n1, int* n2)
{
    int pro=0;
	pro = ((*n1) * (*n2));
	printf("\npro = %d",pro);
}
void div(int* n1, int* n2)
{
    float div=0;
	div = ((*n1) / (*n2));
	printf("\nsum = %f",div);
}